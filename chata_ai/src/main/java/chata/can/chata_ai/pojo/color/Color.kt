package chata.can.chata_ai.pojo.color

class Color(
	val drawerAccentColor: Int,
	val drawerBackgroundColor: Int,
	val drawerBorderColor: Int,
	val drawerHoverColor: Int,
	val drawerColorPrimary: Int,
	val drawerTextColorPlaceholder: Int
)