package chata.can.chata_ai.view.bubbles

interface OnInitializedCallback
{
	fun onInitialized()
}