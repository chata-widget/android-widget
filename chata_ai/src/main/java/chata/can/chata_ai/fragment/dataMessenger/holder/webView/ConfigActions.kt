package chata.can.chata_ai.fragment.dataMessenger.holder.webView

import chata.can.chata_ai.R

object ConfigActions
{
	/**
	 * configActions = 1
	 */
	val biConfig = arrayListOf(
		R.id.ivTable,
		R.id.ivColumn,
		R.id.ivBar,
		R.id.ivLine,
		R.id.ivPie,
		R.id.ivPivot)

	/**
	 * configActions = 2
	 */
	val triReduceConfig = arrayListOf(
		R.id.ivTable,
		R.id.ivColumn,
		R.id.ivBar,
		R.id.ivLine)

	/**
	 * configActions = 3
	 */
	val triConfig = arrayListOf(
		R.id.ivTable,
		R.id.ivColumn,
		R.id.ivBar,
		R.id.ivPivot,
		R.id.ivHeat,
		R.id.ivBubble)

	/**
	 * configActions = 4
	 */
	val biConfigReduce = arrayListOf(
		R.id.ivTable,
		R.id.ivColumn,
		R.id.ivBar,
		R.id.ivLine,
		R.id.ivPie)
}