package chata.can.chata_ai.fragment.exploreQueries

interface ExploreQueriesContract
{
	fun getRelatedQueries(relatedQuery: RelatedQuery)
}