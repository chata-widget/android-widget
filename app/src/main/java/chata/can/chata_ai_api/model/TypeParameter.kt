package chata.can.chata_ai_api.model

enum class TypeParameter
{
	TOGGLE,
	INPUT,
	BUTTON,
	SEGMENT,
	COLOR
}